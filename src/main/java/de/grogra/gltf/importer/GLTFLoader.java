package de.grogra.gltf.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import javax.vecmath.AxisAngle4d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.InputStreamSource;
import de.grogra.pf.io.ObjectSource;
import de.grogra.xl.util.FloatList;
import de.grogra.xl.util.IntList;
import de.javagl.jgltf.model.GltfModel;
import de.javagl.jgltf.model.MeshModel;
import de.javagl.jgltf.model.MeshPrimitiveModel;
import de.javagl.jgltf.model.NodeModel;
import de.javagl.jgltf.model.impl.DefaultMeshModel;
import de.javagl.jgltf.model.io.GltfModelReader;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.*;
import de.grogra.imp3d.objects.MeshNode;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.PolygonMesh;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.RGBAShader;

public class GLTFLoader extends FilterBase implements ObjectSource {

	java.util.HashMap<String,de.grogra.imp3d.shading.Shader> cols;
	public GLTFLoader(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(IOFlavor.NODE);
		cols = new java.util.HashMap<String,de.grogra.imp3d.shading.Shader>();
		
	}

	
	@Override
	public Object getObject() throws IOException {

		Node outRoot = new Node();

		GltfModelReader gltfModelReader = new GltfModelReader();
		GltfModel gltfModel;

		if(source instanceof FileSource) {
			File f = ((FileSource) source).getInputFile();
			gltfModel = gltfModelReader.read(f.toPath());
			String shaderPath = f.toPath().toString().split("\\.")[0]+".csv";
			File sf = new File(shaderPath);
			if(sf.exists()) {
				try (BufferedReader br = new BufferedReader(new FileReader(sf))) {
				    String line;
				    while ((line = br.readLine()) != null) {
				        String[] parts = line.split(",");
						//int id = Integer.parseInt(parts[0]);
			    		float r = Float.parseFloat(parts[1]);
			    		float g = Float.parseFloat(parts[2]);
			    		float b = Float.parseFloat(parts[3]);
			    		float a = Float.parseFloat(parts[4]);
			    		cols.put(parts[0], new RGBAShader(r,g,b,a));
				    }
				}
			}
			
			//			String colorFileName=f.getName().split(".")[0]+".csv";
		}else {
			InputStream in = ((InputStreamSource) source).getInputStream();
			gltfModel = gltfModelReader.readWithoutReferences(in);
		}

		for (NodeModel n : gltfModel.getNodeModels()) {
			if (n.getParent() == null) {
				outRoot.addEdgeBitsTo(getSubGraph(n), Graph.BRANCH_EDGE, null);
			}
		}
		return outRoot;
	}

	private Node getSubGraph(NodeModel in) {
		float[] inp = in.computeLocalTransform(null);
		float[] inpG= {
				inp[0],inp[4],inp[8],inp[12],
				inp[1],inp[5],inp[9],inp[13],
				inp[2],inp[6],inp[10],inp[14],
				inp[3],inp[7],inp[11],inp[15]								
				};
		
		Matrix4d transform = new Matrix4d(new Matrix4f(inpG));
		Null transformationNode = new Null();			
		transformationNode.setTransform(transform );
		PolygonMesh mesh = new PolygonMesh();
		
		for (MeshModel m : in.getMeshModels()) {
			for (MeshPrimitiveModel pm : m.getMeshPrimitiveModels()) {			
				FloatList vertexData = new FloatList();
				FloatBuffer fbuf = pm.getAttributes().get("POSITION").getAccessorData().createByteBuffer().asFloatBuffer();
				while (fbuf.hasRemaining()) {
					vertexData.add(fbuf.get());
				}
				
				
				Object x = pm.getIndices().getAccessorData().getComponentType();
				IntList indices =new IntList();
				if(x ==int.class) {
					IntBuffer buff = pm.getIndices().getAccessorData().createByteBuffer().asIntBuffer();
					while (buff.hasRemaining()) {
						indices.add((int) buff.get());
					}	
				}else if(x==short.class) {
					ShortBuffer buff = pm.getIndices().getAccessorData().createByteBuffer().asShortBuffer();
					while (buff.hasRemaining()) {
						indices.add((int) buff.get());
					}	
				}else if(x==byte.class) {
					ByteBuffer buff = pm.getIndices().getAccessorData().createByteBuffer();
					while (buff.hasRemaining()) {
						indices.add((int) buff.get());
					}	
				}else if(x==long.class) {
					LongBuffer buff = pm.getIndices().getAccessorData().createByteBuffer().asLongBuffer();
					while (buff.hasRemaining()) {
						indices.add((int) buff.get());
					}	
				}
				mesh.setIndexData(indices);
				mesh.setVertexData(vertexData);

			}
		}
		Null meshNode;
		if(mesh.getIndexData()!=null) {
			meshNode =new MeshNode(mesh);
			transformationNode.addEdgeBitsTo(meshNode, Graph.BRANCH_EDGE, null);
			if(cols!=null && cols.containsKey(in.getName())) {
				((MeshNode)meshNode).setShader(cols.get(in.getName()));
			}

		}else {
			meshNode =transformationNode;
		}
		meshNode.setName(in.getName());
				for (NodeModel inChild : in.getChildren()) {
			meshNode.addEdgeBitsTo(getSubGraph(inChild), Graph.BRANCH_EDGE, null);
		}
		return transformationNode;
		}
}
