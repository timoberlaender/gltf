package de.grogra.gltf.exporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.vecmath.Matrix4d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.SceneTree;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTreeWithShader;
import de.grogra.imp3d.objects.Transformation;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.StringMap;
import de.javagl.jgltf.impl.v2.GlTF;
import de.javagl.jgltf.model.creation.GltfModelBuilder;
import de.javagl.jgltf.model.impl.DefaultGltfModel;
import de.javagl.jgltf.model.impl.DefaultNodeModel;
import de.javagl.jgltf.model.impl.DefaultSceneModel;
import de.javagl.jgltf.model.io.GltfWriter;
import de.javagl.jgltf.model.io.v2.GltfAssetV2;
import de.javagl.jgltf.model.io.v2.GltfAssetsV2;

public class GLTFExport extends SceneGraphExport implements FileWriterSource {
	private Workbench workbench = null;
	public static final MetaDataKey<Float> FLATNESS = new MetaDataKey<Float>("flatness");
	static int instantiaion_stemp = 0;
	public Leaf lastRealNode;
	public Matrix4d lastRealTrans;
	public final StringMap nodeRegistry = new StringMap();

	public GLTFExport(FilterItem item, FilterSource source) {
		super(item, source);
		setFlavor(item.getOutputFlavor()); // IOflavor retrieved from filter item each time
		workbench = Workbench.current();

	}

	public int getInstantiation_stemp() {
		instantiaion_stemp += 1;
		return instantiaion_stemp;
	}

	@Override
	public void write(File file) throws IOException {
		workbench.beginStatus(this);
		workbench.setStatus(this, "Export GLTF", -1);

		write();
		DefaultSceneModel sceneModel = new DefaultSceneModel();

		DefaultNodeModel root = new DefaultNodeModel();
	//	root.setRotation(new float[] { -1, 0, 0, 1 });
		for (String k : nodeRegistry.getKeys()) {
			GLTFNodeWrapper gnw = (GLTFNodeWrapper) nodeRegistry.get(k);
			if (gnw.getParentId() == -1) {
				root.addChild(gnw.getInNode());
			} else {
				GLTFNodeWrapper parent = (GLTFNodeWrapper) nodeRegistry.get("" + gnw.getParentId());

				if (parent != null && parent.outNode != null) {
					parent.addChild(gnw);
				} else {
					root.addChild(gnw.inNode);
				}
			}
		}
		sceneModel.addNode(root);
		GltfModelBuilder gltfModelBuilder = GltfModelBuilder.create();
		gltfModelBuilder.addSceneModel(sceneModel);
		DefaultGltfModel gltfModel = gltfModelBuilder.build();
		GltfAssetV2 asset = GltfAssetsV2.createEmbedded(gltfModel);
		GlTF gltf = asset.getGltf();

		GltfWriter gltfWriter = new GltfWriter();
		gltfWriter.setIndenting(true);
		FileOutputStream out = new FileOutputStream(file);
		gltfWriter.write(gltf, out);
		workbench.clearStatusAndProgress(this);
	}

	@Override
	protected SceneTree createSceneTree(View3D scene) {
		SceneTree t = new SceneTreeWithShader(scene) {

			@Override
			protected boolean acceptLeaf(Object object, boolean asNode) {
				return getExportFor(object, asNode) != null;
			}

			@Override
			protected Leaf createLeaf(Object object, boolean asNode, long id) {
				Leaf l = new Leaf(object, asNode, id);
				init(l);
				return l;
			}
		};
		t.createTree(true, ((Boolean) workbench.getProperty(Workbench.EXPORT_VISIBLE_LAYER) == null) ? false
				: (Boolean) workbench.getProperty(Workbench.EXPORT_VISIBLE_LAYER));
		return t;
	}

	@Override
	protected void beginGroup(InnerNode group) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	protected void endGroup(InnerNode group) throws IOException {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public NodeExport getExportFor(Object object, boolean asNode) {
		if (object instanceof Node && ((Node) object).getIgnored()) {
			return null;
		}

		// Boolean b = ((Node) object)).getGCMark();

		Object s = getGraphState().getObjectDefault(object, asNode, Attributes.SHAPE, null);
		Object t = getGraphState().getObjectDefault(object, asNode, Attributes.TRANSFORMATION, null);

		if (s != null) {
			NodeExport ex = super.getExportFor(s, asNode);
			if (ex != null) {
				return ex;
			}
			return super.getExportFor(object, asNode);

		}
		if (t != null) {
			NodeExport ex = super.getExportFor(t, asNode);
			if (ex != null) {
				return ex;
			}
			return super.getExportFor(object, asNode);

		}
		if ((!(object instanceof Null)) && (!(object instanceof Transformation))) {
			return super.getExportFor(object, asNode);
		}
		return null;
	}

}