package de.grogra.gltf.exporter.objects;

import javax.vecmath.Point3d;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Axis;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.IMP3D;
import de.grogra.imp3d.PolygonArray;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class Cylinder extends AxisBase{

	@Override
	public void createMesh(Leaf node) {
		Point3d pos = new Point3d ();
		float r =  node.getFloat (Attributes.RADIUS);
//				((de.grogra.imp3d.objects.Cylinder)node.object).getRadius();
		double l = node.getDouble (Attributes.LENGTH);
//		double l = ((Axis)node.object).getLength();
		// prepare polygon array for indexed geometry
		// store 3d quads in this array
		PolygonArray p = new PolygonArray ();
		p.dimension = 3;
		p.edgeCount = 4;

		Item general = Item.resolveItem (Workbench.current (), "/export/gltf");
		final int uCount = Utils.getInt(general, "cylinderucount", 15);

		// generate geometry
		int index = 2;
		pos.set (0, 0, 0);
		p.vertices.push ((float) pos.x).push ((float) pos.y).push ((float) pos.z);
		pos.set (0, 0, l);
		p.vertices.push ((float) pos.x).push ((float) pos.y).push ((float) pos.z);
		for (int u = 0; u < uCount; u++) {
			float phi = (float) (Math.PI * 2 * u / uCount);
			float cosPhi = (float) Math.cos (phi);
			float sinPhi = (float) Math.sin (phi);

			Point3d p0 = new Point3d (cosPhi, sinPhi, 0);
			p0.scale (r);
			Point3d p1 = new Point3d (cosPhi, sinPhi, l/r);
			p1.scale (r);
	
			p.vertices.push ((float) p0.x).push ((float) p0.y).push ((float) p0.z);
			p.vertices.push ((float) p1.x).push ((float) p1.y).push ((float) p1.z);

			// bottom face
			if (Utils.getBoolean(general, "cylindertopbottom"))
				p.polygons.push (0).push (index + 2 * (uCount - u > 1 ? 1 : 1 - uCount)).push (index).push (0);
			// top face
			if (Utils.getBoolean(general, "cylindertopbottom"))
				p.polygons.push (1).push (index + 1).push (index + 1  + 2 * (uCount - u > 1 ? 1 : 1 - uCount)).push (1);
			// mantle face
			p.polygons.push (index).push (index+2 + 2 * (uCount - u > 1 ? 0 : 0 - uCount)).push (
					index + 3 + 2 * (uCount - u > 1 ? 0 : 0 - uCount)).push (index + 1);

			index += 2;
		}

		// write object
		mesh2(p, node, true);
	}
	

}
