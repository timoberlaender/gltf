package de.grogra.gltf.exporter.objects;

import java.io.IOException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;

import de.grogra.gltf.exporter.GLTFExport;
import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.graph.ArrayPath;
import de.grogra.graph.Graph;
import de.grogra.graph.impl.Edge;
import de.grogra.imp3d.io.SceneGraphExport;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.SceneTree.InnerNode;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.objects.Transformation;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public abstract class ObjectBase implements SceneGraphExport.NodeExport {

	GLTFExport glExp;

	@Override
	public void export(Leaf node, InnerNode transform, SceneGraphExport sge) throws IOException {
		glExp = (GLTFExport) sge;
		de.grogra.graph.impl.Node n = (de.grogra.graph.impl.Node) node.object;
		long id = n.getId();
		long pId = -1;
		DefaultNodeModel inTmp = null;
		if (id == -1) {

			de.grogra.graph.impl.Node pN = (de.grogra.graph.impl.Node) glExp.lastRealNode.object;
			pId = pN.getId();
			id = pId + (glExp.getInstantiation_stemp() * 1000000000);
			System.err.println(id);
			Matrix4d thismd = new Matrix4d(
					GlobalTransformation.get(n, true, glExp.getGraphState(), false).toMatrix4d());
			Matrix4d ind = new Matrix4d(glExp.lastRealTrans);
			ind.invert();
			ind.mul(thismd);
			Matrix4f thism = new Matrix4f(ind);

			inTmp = new DefaultNodeModel();
			inTmp.setMatrix(new float[] { thism.m00, thism.m10, thism.m20, thism.m30, thism.m01, thism.m11, thism.m21,
					thism.m31, thism.m02, thism.m12, thism.m22, thism.m32, thism.m03, thism.m13, thism.m23,
					thism.m33 });
		} else {
			glExp.lastRealNode = node;
			glExp.lastRealTrans = new Matrix4d(
					GlobalTransformation.get(n, true, glExp.getGraphState(), true).toMatrix4d());
			for (Edge e = n.getFirstEdge(); e != null; e = e.getNext(n)) {
				if (e.isTarget(n) && e.getEdgeBits() != Graph.REFINEMENT_EDGE) {
					pId = e.getSource().getId();
				}
			}
		}
		GLTFNodeWrapper wrapper = new GLTFNodeWrapper(id, pId);
		if (inTmp != null) {
			wrapper.instantiation = true;
			wrapper.setInNode(inTmp);

		}
		glExp.nodeRegistry.put("" + id, wrapper);
		exportImpl(node, wrapper);
	}

	public abstract void exportImpl(Leaf node, GLTFNodeWrapper wrapper);
}
