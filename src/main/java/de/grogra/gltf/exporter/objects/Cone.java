package de.grogra.gltf.exporter.objects;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class Cone extends AxisBase{

	@Override
	public void createMesh(Leaf node) {
		Point3d pos = new Point3d ();
		float r = node.getFloat (Attributes.RADIUS);
		double l = node.getDouble (Attributes.LENGTH);

		// prepare polygon array for indexed geometry
		// store 3d triangles in this array
		PolygonArray p = new PolygonArray ();
		p.dimension = 3;
		p.edgeCount = 3;

		Item general = Item.resolveItem (Workbench.current (), "/export/gltf");
		final int uCount = Utils.getInt(general, "coneucount", 15);

		// generate geometry
		int index = 2;
		pos.set(0, 0, 0);
		p.vertices.push ((float)pos.x).push ((float)pos.y).push ((float)pos.z);
		pos.set(0, 0, l);
		p.vertices.push ((float)pos.x).push ((float)pos.y).push ((float)pos.z);
		for (int u = 0; u < uCount; u++) {
			float phi = (float) (Math.PI * 2 * u / uCount);
			float cosPhi = (float) Math.cos (phi);
			float sinPhi = (float) Math.sin (phi);
			pos.set (cosPhi, sinPhi, 0);
			pos.scale (r);
			p.vertices.push ((float) pos.x).push ((float) pos.y).push ((float) pos.z);
			if (Utils.getBoolean(general, "conebottom"))
				p.polygons.push (0).push (index + (uCount - u > 1 ? 1 : 1 - uCount)).push (index);
			p.polygons.push (index).push (index + (uCount - u > 1 ? 1 : 1 - uCount)).push (1);
			index++;
		}

		// write object
		mesh2(p, node, false);
	}
}
