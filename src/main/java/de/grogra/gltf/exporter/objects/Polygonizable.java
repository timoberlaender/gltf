package de.grogra.gltf.exporter.objects;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import de.grogra.gltf.exporter.GLTFExport;
import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SphereSegment;
import de.grogra.imp3d.objects.SceneTree.Leaf;

public class Polygonizable extends ShadedBase {

	@Override
	public void createMesh(Leaf node) {
		Point3d pos = new Point3d ();
		float flatness = glExp.getMetaData (GLTFExport.FLATNESS, 1f);
		int flags = 0;

		PolygonArray p = new PolygonArray ();
		
		// TODO why this line ?
		glExp.getGraphState ().setObjectContext (node.object, node.asNode);
		
		GraphState gs = glExp.getGraphState ();
		de.grogra.imp3d.Polygonizable surface = (de.grogra.imp3d.Polygonizable) node.getObject (Attributes.SHAPE);
		ContextDependent source = surface.getPolygonizableSource (gs);
		surface.getPolygonization ().polygonize (source, gs, p, flags, flatness);

		// transform the mesh
		for (int i = 0; i < p.vertices.size(); i+=3) {
			pos.x = p.vertices.get(i+0);
			pos.y = p.vertices.get(i+1);
			pos.z = p.vertices.get(i+2);
			
			p.vertices.set(i+0, (float)pos.x);
			p.vertices.set(i+1, (float)pos.y);
			p.vertices.set(i+2, (float)pos.z);
		}

		// write object
		if (node.object instanceof SphereSegment) {
			mesh2(p, node, false);
		} else {
			mesh2(p, node, true);
		}
	}

}
