package de.grogra.gltf.exporter.objects;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.Axis;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public abstract class AxisBase extends ShadedBase {

	@Override
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		float l = (float) node.getDouble (Attributes.LENGTH);
		DefaultNodeModel in= drawObject(node);
		DefaultNodeModel out= new DefaultNodeModel();
		wrapper.setInNode(in);
		if(((Null)node.object).isTransforming()) {
			out.setTranslation(new float[] {0,0,l});
			in.addChild(out);
			wrapper.setOutNode(out);
		}else {
			wrapper.setOutNode(in);
		}
	}
	
}
