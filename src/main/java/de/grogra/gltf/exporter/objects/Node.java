package de.grogra.gltf.exporter.objects;

import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.GlobalTransformation;
import de.grogra.imp3d.objects.Transformation;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public class Node extends ObjectBase{

	@Override
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		DefaultNodeModel in= new DefaultNodeModel();

		de.grogra.graph.impl.Node n = (de.grogra.graph.impl.Node) node.object;
		
		if(n instanceof Transformation) {
			Matrix4d maIn = new Matrix4d();
			Matrix4d maout = new Matrix4d();
			maIn.setIdentity();
			maout.setIdentity();
			
			((Transformation)n).postTransform(n, false, maIn, maout, maout, glExp.getGraphState());
			Matrix4f thism = new Matrix4f(maout);
			in.setMatrix(new float[] {thism.m00,thism.m10,thism.m20,thism.m30,
					thism.m01,thism.m11,thism.m21,thism.m31,
					thism.m02,thism.m12,thism.m22,thism.m32,
					thism.m03,thism.m13,thism.m23,thism.m33});
		}

//		Matrix4d thismd = GlobalTransformation.get(n, true, glExp.getGraphState(), false).toMatrix4d();	
		wrapper.setInNode(in);
		wrapper.setOutNode(in);
	}
}
