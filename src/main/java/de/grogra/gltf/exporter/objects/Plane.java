package de.grogra.gltf.exporter.objects;

import javax.vecmath.Point4d;

import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class Plane extends ShadedBase{

	@Override
	public void createMesh(Leaf node) {
		// TODO Auto-generated method stub

		float w = 10;
		float l = 10;

		double x0 = -w/2d;
		double y0 = -l/2d;
		double x1 = w/2d;
		double y1 = l/2d;

	
		Point4d[] p = new Point4d[] {
				new Point4d (x0, y0, 0, 1), new Point4d (x0, y1, 0, 1),
				new Point4d (x1, y0, 0, 1), new Point4d (x1, y1, 0, 1)};

		Item general = Item.resolveItem (Workbench.current (), "/export/gltf");
		
		// write object
		writeVertices(p);
		if (Utils.getBoolean(general, "planetwofaces")) {
			writeFacets(new String[] {
				//to make it visible from both sides: double the facets with opposite order
				"1 0 2", "1 2 3", //one side
				"1 2 0", "1 3 2", //other side
			});
		} else {
			writeFacets(new String[] {
					//only one side
					"1 0 2", "1 2 3", //one side
				});
		}
	}

	

}
