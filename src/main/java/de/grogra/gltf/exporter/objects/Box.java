package de.grogra.gltf.exporter.objects;

import javax.vecmath.Point4d;

import de.grogra.imp3d.objects.SceneTree.Leaf;

public class Box extends AxisBase {

	@Override
	public void createMesh(Leaf node) {		
/*
		float l = ((de.grogra.imp3d.objects.Box)node.object).getLength();
		float w = ((de.grogra.imp3d.objects.Box)node.object).getWidth();
		float h = ((de.grogra.imp3d.objects.Box)node.object).getHeight();
		float x0 = -w / 2f;
		float y0 = -h / 2f;
		float z0 = 0;
		float x1 = w / 2f;
		float y1 = h / 2f;
		float z1 = l;
		
		
		
		
	int indices[] = { 1, 0, 2,
			1, 2, 3,
			3, 2, 4,
			3, 4, 5,
			5, 4, 6,
			5, 6, 7,
			7, 6, 0,
			7, 0, 1,
			4, 2, 0,
			0, 6, 4,
			1, 3, 5,
			5, 7, 1};
	
	float positions[] = {
			x0, y0, z0,
			x0, y0, z1,
			x1, y0, z0,
			x1, y0, z1,
			x1, y1, z0,
			x1, y1, z1,
			x0, y1, z0,
			x0, y1, z1	
	};
	
	float normals[] = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
	float texCoords0[] = { 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, };

	MeshPrimitiveBuilder meshPrimitiveBuilder = MeshPrimitiveBuilder.create();
	meshPrimitiveBuilder.setIntIndicesAsShort(IntBuffer.wrap(indices));
	meshPrimitiveBuilder.addPositions3D(FloatBuffer.wrap(positions));
	//meshPrimitiveBuilder.addNormals3D(FloatBuffer.wrap(normals));
//	meshPrimitiveBuilder.addTexCoords02D(FloatBuffer.wrap(texCoords0));
//	meshPrimitiveBuilder.addAttribute("TEXCOORD_1", AccessorModels.createFloat2D(FloatBuffer.wrap(texCoords0)));
	DefaultMeshPrimitiveModel meshPrimitiveModel = meshPrimitiveBuilder.build();
	DefaultMeshModel meshModel = new DefaultMeshModel();
	meshModel.addMeshPrimitiveModel(meshPrimitiveModel);
	return meshModel;
	}
	
*/

		float l = ((de.grogra.imp3d.objects.Box)node.object).getLength();
		float w = ((de.grogra.imp3d.objects.Box)node.object).getWidth();
		float h = ((de.grogra.imp3d.objects.Box)node.object).getHeight();

		double x0 = -w / 2d;
		double y0 = -h / 2d;
		double z0 = 0;
		double x1 = w / 2d;
		double y1 = h / 2d;
		double z1 = l;

		Point4d[] p = new Point4d[] {new Point4d (x0, y0, z0, 1),
				new Point4d (x0, y0, z1, 1), new Point4d (x1, y0, z0, 1),
				new Point4d (x1, y0, z1, 1), new Point4d (x1, y1, z0, 1),
				new Point4d (x1, y1, z1, 1), new Point4d (x0, y1, z0, 1),
				new Point4d (x0, y1, z1, 1)};


		//generate colour string
		//getColorString(node);
		
		// write object
		writeVertices(p);
		writeFacets(new String[] {
			"1 0 2",
			"1 2 3",
			"3 2 4",
			"3 4 5",
			"5 4 6",
			"5 6 7",
			"7 6 0",
			"7 0 1",
			"4 2 0",
			"0 6 4",
			"1 3 5",
			"5 7 1"});
	}
		
}
