package de.grogra.gltf.exporter.objects;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public class Translate extends ObjectBase{

	@Override
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		DefaultNodeModel tra= new DefaultNodeModel();
		
		de.grogra.turtle.Translate ta = (de.grogra.turtle.Translate) node.object;
		float[] xyz = { ta.translateX,ta.translateY,ta.translateZ};
		tra.setTranslation(xyz);
		wrapper.setInNode(tra);
		wrapper.setOutNode(tra);
	
	}

}
