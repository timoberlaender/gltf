package de.grogra.gltf.exporter.objects;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public class TurtleM extends ObjectBase{

	@Override
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		float l = (float) node.getDouble (Attributes.LENGTH);
		DefaultNodeModel in= new DefaultNodeModel();
		in.setTranslation(new float[] {0,0,l});
		wrapper.setInNode(in);
		wrapper.setOutNode(in);
	}
}
