package de.grogra.gltf.exporter.objects;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point4d;
import javax.vecmath.Vector3f;

import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.Utils;

public class Parallelogram extends AxisBase {

	@Override
	public void createMesh(Leaf node) {
		
		float l = (float) node.getDouble (Attributes.LENGTH);
		Vector3f a = (Vector3f) node.getObject (Attributes.AXIS);

		double x0 = -a.x;
		double x1 = a.x;
		double z1 = l;



		Point4d[] p = new Point4d[] {
				new Point4d (x0, 0, 0, 1), new Point4d (x0, 0, z1, 1),
				new Point4d (x1, 0, 0, 1), new Point4d (x1, 0, z1, 1)};

		
		//generate colour string
	//	getColorString(node);
		
		Item general = Item.resolveItem (Workbench.current (), "/export/gltf");

		// write object
		writeVertices(p);
		if (Utils.getBoolean(general, "parallelogramtwofaces")) {
			writeFacets(new String[] {
				//to make it visible from both sides: double the facets with opposite order
				"1 0 2", "1 2 3", //one side
				"1 2 0", "1 3 2", //other side
			});
		} else {
			writeFacets(new String[] {
					//only one side
					"1 0 2", "1 2 3", //one side
				});
		}
	}
}
