package de.grogra.gltf.exporter.objects;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point4d;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.objects.Attributes;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.math.Transform3D;
import de.grogra.pf.io.FilterSource.MetaDataKey;
import de.javagl.jgltf.model.creation.MeshPrimitiveBuilder;
import de.javagl.jgltf.model.impl.DefaultMeshModel;
import de.javagl.jgltf.model.impl.DefaultMeshPrimitiveModel;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public abstract class ShadedBase extends ObjectBase {


	private ArrayList<Integer> indices;
	private ArrayList<Float> positions;
	
	int[] tmpIdx;
	
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		ShadedNull sn = (ShadedNull) node.object;
		
		DefaultNodeModel in= drawObject(node);
		wrapper.setInNode(in);
		wrapper.setOutNode(in);
		
	}
	
	
	public DefaultNodeModel drawObject(Leaf node) {
		indices=new ArrayList<Integer>();
		positions=new ArrayList<Float>();

		createMesh(node);
		

		DefaultNodeModel t = new DefaultNodeModel();
		Transform3D oo = (Transform3D)node.getObject(Attributes.TRANSFORM);
		if(oo!=null) {
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		Matrix4d nd = new Matrix4d();
		oo.transform(m, nd);
		Matrix4f n = new Matrix4f(nd);
		t.setMatrix(new float[] { n.m00, n.m10, n.m20, n.m30, n.m01, n.m11, n.m21,
					n.m31, n.m02, n.m12, n.m22, n.m32, n.m03, n.m13, n.m23,
					n.m33 });
		}
		
		MeshPrimitiveBuilder meshPrimitiveBuilder = MeshPrimitiveBuilder.create();
		 int[] ind = new int[indices.size()];
		 int i=0;
		 for(Object o : indices) {
			 ind[i]=(int)o;
			 i++;
		 }
		 i=0;
		 float[] pos = new float[positions.size()];
		 for(Object o : positions) {
			 pos[i]=(float)o;
			 i++;
		 }
		meshPrimitiveBuilder.setIntIndicesAsShort(IntBuffer.wrap(ind));
		meshPrimitiveBuilder.addPositions3D(FloatBuffer.wrap(pos));
		DefaultMeshPrimitiveModel meshPrimitiveModel = meshPrimitiveBuilder.build();
		DefaultMeshModel meshModel = new DefaultMeshModel();
		meshModel.addMeshPrimitiveModel(meshPrimitiveModel);
		
		t.addMeshModel(meshModel);
		return t;	
		
	}

	public abstract void createMesh(Leaf node);

	
	protected void writeVertices(Point4d[] ps) {
		tmpIdx = new int[ps.length];
		int i=0;
		for(Point4d p : ps) {
			positions.add((float) p.x);
			positions.add((float) p.y);
			positions.add((float) p.z);
			
			tmpIdx[i] = (positions.size()/3)-1;
			i++;
		}
	}
	protected void writeFacets(String[] fs) {
		for(String f : fs) {
			for(String i : f.split(" ")){
				indices.add(tmpIdx[Integer.parseInt(i)]);
			}
		}
	}
	
	void mesh2 (PolygonArray p, Leaf node, boolean twoF) {

		//generate colour string
		
		if (p.polygons.size == 0) return;
		int v1,v2,v3;
		if (p.edgeCount==3) {
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				
				//first facet of the rectangle
				Point4d[] pA = new Point4d[] {
					new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
					new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1),
					new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1)
				};
					writeVertices(pA);
					writeFacets(new String[] {"0 1 2"});
				if(twoF) {
					//second facet of the rectangle
					pA = new Point4d[] {
						new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
						new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1),
						new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1)
					};
						writeVertices(pA);
						writeFacets(new String[] {"0 1 2"});
				}
			}
		}

		if (p.edgeCount==4) {
			int v4;
			// output indices
			for (int i = 0; i < p.polygons.size (); i += p.edgeCount) {
				v1 = p.polygons.get (i + 0);
				v2 = p.polygons.get (i + 1);
				v3 = p.polygons.get (i + 2);
				v4 = p.polygons.get (i + 3);
				
				//first facet of the rectangle
				Point4d[] pA = new Point4d[] {
					new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
					new Point4d(p.vertices.get(v2*p.dimension+0),p.vertices.get(v2*p.dimension+1),p.vertices.get(v2*p.dimension+2),1),
					new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1)
				};

					writeVertices(pA);
					writeFacets(new String[] {"0 1 2"});
				
				if(twoF) {
					//second facet of the rectangle
					pA = new Point4d[] {
						new Point4d(p.vertices.get(v1*p.dimension+0),p.vertices.get(v1*p.dimension+1),p.vertices.get(v1*p.dimension+2),1),
						new Point4d(p.vertices.get(v3*p.dimension+0),p.vertices.get(v3*p.dimension+1),p.vertices.get(v3*p.dimension+2),1),
						new Point4d(p.vertices.get(v4*p.dimension+0),p.vertices.get(v4*p.dimension+1),p.vertices.get(v4*p.dimension+2),1)
					};
						writeVertices(pA);
						writeFacets(new String[] {"0 1 2"});
				}
			}
		}
	}
	
}
