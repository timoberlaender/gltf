package de.grogra.gltf.exporter.objects;

import de.grogra.gltf.exporter.GLTFNodeWrapper;
import de.grogra.imp3d.objects.SceneTree.Leaf;
import de.javagl.jgltf.model.impl.DefaultNodeModel;

public class TurtleRotation extends ObjectBase{

	@Override
	public void exportImpl(Leaf node, GLTFNodeWrapper wrapper) {
		DefaultNodeModel ro= new DefaultNodeModel();
		float angle = ((de.grogra.turtle.Rotation)node.object).getAngle();
		angle = (float)Math.toRadians(angle);
		float w = (float)Math.sin(angle);
		float x=0,y=0,z=0;
		if(node.object instanceof de.grogra.turtle.RL) {
			x=1;
		}
		if(node.object instanceof de.grogra.turtle.RH) {
			z=1;
		}
		if(node.object instanceof de.grogra.turtle.URotation) {
			y=1;
		}	
		
		// calculate quaternion rotations
		float[] rotation= {
				(float)Math.sin(angle/2)*x,
				(float)Math.sin(angle/2)*y,
				(float)Math.sin(angle/2)*z,
				(float)Math.cos(angle/2)
		};
		
		ro.setRotation(rotation);
		wrapper.setInNode(ro);
		wrapper.setOutNode(ro);
	}
}
