package de.grogra.gltf.exporter;

import de.javagl.jgltf.model.impl.DefaultNodeModel;

public class GLTFNodeWrapper {
	DefaultNodeModel inNode;
	DefaultNodeModel outNode;
	long parentId;
	long id;
	public boolean instantiation;
	
	public GLTFNodeWrapper(long id, long parentId) {
		this.id=id;
		this.parentId=parentId;
		this.instantiation=false;
	}

	public long getParentId() {
		return parentId;
	}

	public long getId() {
		return id;
	}
	
	public DefaultNodeModel getInNode() {
		return inNode;
	}

	public void setInNode(DefaultNodeModel inNode) {
		if(this.inNode!=null) {
			this.inNode.addChild(inNode);
		}else {
			this.inNode = inNode;
			this.inNode.setName(""+getId());
			
		}
	}

	public DefaultNodeModel getOutNode() {
		return outNode;
	}

	public void setOutNode(DefaultNodeModel outNode) {
		this.outNode = outNode;
		if(inNode!=outNode) {
			this.outNode.setName(""+getId()+"_b");
		}
	}
	public void addChild(GLTFNodeWrapper c) {
		if((!this.instantiation) && c.instantiation) {
			inNode.addChild(c.getInNode());
		}else {
			outNode.addChild(c.getInNode());		
		}
	}
}
