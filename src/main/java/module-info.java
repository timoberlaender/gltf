module gltf{
	exports de.grogra.gltf.exporter;
	exports de.grogra.gltf.importer;
	
	requires platform.core;
	requires platform;
	requires graph;
	requires jgltf.impl.v2;
	requires jgltf.model;
	requires vecmath;
	requires jgltf.model.builder;
	requires math;
	requires utilities;
	requires xl.core;
	requires imp;
	requires transitive imp3d;
	requires rgg;


}
